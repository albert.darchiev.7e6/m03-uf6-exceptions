import java.lang.NumberFormatException
import java.util.*

//Pasar a Double o 1.0
fun main() {
    val scanner = Scanner(System.`in`)
    println("INPUT: ")
    val scan = scanner.next()
    println(aDoubleoU(scan))
}

fun aDoubleoU(input: String): Double{
    try {
        return input.toDouble()
    }catch (e: NumberFormatException){
        return 1.0
    }
}