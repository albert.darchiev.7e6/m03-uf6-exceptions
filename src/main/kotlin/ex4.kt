import java.util.NoSuchElementException

//Aplicació lliure
fun main(){
    try {
        val numbersList =  mutableListOf<Int>()
        numbersList.first()
    }
    catch (e: NoSuchElementException){
        println("ERROR -> "+ e.message)
        }

    try {
        val numbersListAsMutable = listOf(1, 2) as MutableList
        numbersListAsMutable.add(0)
    }
    catch (e: UnsupportedOperationException){
        println("ERROR2 -> "+ e.message)
        }
}