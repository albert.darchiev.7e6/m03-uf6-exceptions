import java.util.Scanner

//Divideix o Cero
fun main() {
val scanner = Scanner(System.`in`)
println("INPUT: (\"num1\",\"num2\")")
    val scan = scanner.next().split(",")

    println( divideixoCero(scan[0].toInt(),scan[1].toInt()) )
}

fun divideixoCero(dividend: Int, divisor: Int): Int{
    try {
        return dividend / divisor
    } catch (e: ArithmeticException) {
        return 0
    }
}