import java.io.File
import java.io.IOException

//Llegir un fitxer
fun main(){
    val file = File("src/main/kotlin/ex3testFile")

    try {
        println(file.readText())
    }catch (e:IOException){
        println("S'ha produït un error d'entrada/sortida:")
        println("ERROR -> " + e.message)
    }

}